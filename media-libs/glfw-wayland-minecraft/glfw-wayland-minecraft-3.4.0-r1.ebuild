# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake-multilib

DESCRIPTION="Portable OpenGL FrameWork (wayland, patched for minecraft)"
HOMEPAGE="https://github.com/Admicos/minecraft-wayland"
SRC_URI="https://github.com/glfw/glfw/archive/62e175ef9fae75335575964c845a302447c012c7.tar.gz -> ${P}.tar.gz"

S="${WORKDIR}/glfw-62e175ef9fae75335575964c845a302447c012c7"
LICENSE="ZLIB"
SLOT="0"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~ppc64 ~riscv ~x86"


RDEPEND="
	media-libs/libglvnd[${MULTILIB_USEDEP}]
	dev-libs/wayland[${MULTILIB_USEDEP}]"
DEPEND="
	${RDEPEND}
	gui-libs/libdecor
	dev-libs/wayland-protocols
	x11-libs/libxkbcommon[${MULTILIB_USEDEP}]"
BDEPEND="
	gui-libs/libdecor
	dev-util/wayland-scanner
	kde-frameworks/extra-cmake-modules"

src_prepare() {
	patch -p1 -d "${S}" < "${FILESDIR}/${P}-0003-Don-t-crash-on-calls-to-focus-or-icon.patch"
	patch -p1 -d "${S}" < "${FILESDIR}/${P}-0004-wayland-fix-broken-opengl-screenshots-on-mutter.patch"
	patch -p1 -d "${S}" < "${FILESDIR}/${P}-0005-Add-warning-about-being-an-unofficial-patch.patch"
	patch -p1 -d "${S}" < "${FILESDIR}/${P}-0007-Platform-Prefer-Wayland-over-X11.patch"

	default
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DGLFW_BUILD_EXAMPLES=no
		-DGLFW_USE_WAYLAND=ON
		-DBUILD_SHARED_LIBS=ON
	)

	cmake-multilib_src_configure
}
