# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MODULES_KERNEL_MIN=5.15 # there is no documentation about it, so i choose that one

EGIT_REPO_URI="https://github.com/Fred78290/nct6687d.git"
inherit git-r3
inherit linux-mod-r1

DESCRIPTION="Nuvoton NCT6687-R firmware"
HOMEPAGE="https://github.com/Fred78290/nct6687d"
SRC_URI="https://github.com/Fred78290/nct6687d"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

DEPEND="sys-kernel/linux-firmware"
RDEPEND="${DEPEND}"
BDEPEND="dev-build/automake sys-devel/gcc"

src_unpack() {
	git-r3_src_unpack
}

src_compile() {
	local modlist=(
		nct6687=kernel/drivers/hwmon:.:${KV_FULL}
	)
	local modargs=( kver="${KV_FULL}" )

	linux-mod-r1_src_compile
}

