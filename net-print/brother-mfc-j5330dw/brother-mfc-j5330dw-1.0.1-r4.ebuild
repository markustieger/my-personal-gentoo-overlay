# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit rpm

DESCRIPTION="LPR and CUPS driver for the Brother MFC-J5330DW"
HOMEPAGE="http://support.brother.com/g/b/producttop.aspx?c=gb&lang=en&prod=mfcj5330dw_us_eu_as"
SRC_URI="http://download.brother.com/welcome/dlf103003/mfcj5330dwlpr-${PV}-0.i386.rpm
http://download.brother.com/welcome/dlf103027/mfcj5330dwcupswrapper-${PV}-0.i386.rpm"

LICENSE="brother-cupswrapper brother-lpr"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="net-print/cups app-text/ghostscript-gpl app-text/poppler app-text/a2ps app-text/psutils"
RDEPEND="${DEPEND}"
BDEPEND=""

S="${WORKDIR}"

src_unpack() {
	rpm_src_unpack ${A}
}

src_prepare() {
	default

	# do not install in /usr/local
	if [ -d "${WORKDIR}"/usr/local/Brother ]; then
		install -d "${WORKDIR}"/usr/share
		mv "${WORKDIR}"/usr/local/Brother/ "${srcdir}"/usr/share/Brother
		rm -rf "${WORKDIR}"/usr/local
		sed -i 's|/usr/local/Brother|/usr/share/Brother|g' `grep -lr '/usr/local/Brother' ./`
	fi

	# setup cups directories
	install -d "${WORKDIR}"/usr/share/cups/model
	install -d "${WORKDIR}"/usr/lib/cups/filter

	# patch an error (?) in the perl script from brother
	patch -p1 "${WORKDIR}"/opt/brother/Printers/mfcj5330dw/cupswrapper/brother_lpdwrapper_mfcj5330dw < "${FILESDIR}"/brother_lpdwrapper_mfcj5330dw.patch

	# go to the cupswrapper directory and find the source file from which to generate a ppd and wrapperfile
	cd `find "${WORKDIR}" -type d -name 'cupswrapper'`

	if [ -f cupswrapper* ]; then
		_wrapper_source=`ls cupswrapper*`

		sed -i '/^\/etc\/init.d\/cups/d' $_wrapper_source
		sed -i '/^sleep/d' $_wrapper_source
		sed -i '/^lpadmin/d' $_wrapper_source
		sed -i 's|/usr|$srcdir/usr|g' $_wrapper_source
		sed -i 's|/opt|$srcdir/opt|g' $_wrapper_source
		sed -i 's|/model/Brother|/model|g' $_wrapper_source
		sed -i 's|lpinfo|echo|g' $_wrapper_source

		export srcdir="${WORKDIR}"
		./$_wrapper_source

		sed -i 's|$srcdir||' "${WORKDIR}"/usr/lib/cups/filter/*lpdwrapper*
		sed -i "s|$srcdir||" "${WORKDIR}"/usr/lib/cups/filter/*lpdwrapper*

		rm $_wrapper_source
	fi

	# /etc/printcap is managed by cups
	rm `find "${WORKDIR}" -type f -name 'setupPrintcap*'`

	# patch filter for PDF printing to avoid error:
	# ERROR: typecheck
	# OFFENDING COMMAND: resourcestatus
	sed -i 's|pdf2ps|pdftocairo -q -ps|g' "${WORKDIR}"/opt/brother/Printers/mfcj5330dw/lpd/filter_mfcj5330dw

	mv "${WORKDIR}"/usr/lib "${WORKDIR}"/usr/libexec
}

src_install() {
	cp -R "${WORKDIR}"/usr "${D}"
	cp -R "${WORKDIR}"/opt "${D}"
}
